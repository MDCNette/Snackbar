# MDCNette | [Snackbar](https://github.com/material-components/material-components-web/tree/master/packages/mdc-snackbar)
Material components SNACKBAR implementation to NETTE Framework.

*Idea behind was inspired by [Ipublikuj](https://github.com/iPublikuj/confirmation-dialog).*
## Install
The best way to get MDCNette dialog is via composer.
```
php composer.phar require mdcnette/snackbar
```
Add MDC Snackbar extensions to your config file
```neon
extensions:
    mdcsnackbar: MDCNette\Snackbar\DI\SnackbarExtension
```
Then in your presenter (_typically in BasePresenter_) add this.
```php
use TSnackbar;
```
Add this to your @layout.latte.
```html
{snippet snackbar} // if you want use ajax. wrap control to snippet
    {control snackbar}
{/snippet}
``` 

### Client side (js/css)
And finally you need to setup javascript...
The best approach is to install it with npm.
```npm
npm install @mdcnette/snackbar
```
*or you can find it in this repo in `client-side` folder.*

in `dist` folder you can find files to include.

- `material-components-web.min.css` 

*(styles for snackbar)* link this to your head in html layout.
- `material-components-web.min.js` 

*(base js for snackbar)* add this to your script load.

- `mdcnette.ajax.snackbar.js` /or/ `mdcnette.noajax.snackbar.js`

*(js for snackbar)* add this to you script load.

@layout.latte preview
```html
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="path/to/material-components-web.min.css" type="text/css">
</head>

<body>
    {include content}
    
    {snippet snackbar} // use snippet tag only if you want use ajax.
        {control snackbar}
    {/snippet snackbar}
    
    <script src="path/to/material-components-web.min.js" type="text/javascript"></script>
    
    <!--IF AJAX-->
    <script src="path/to/nette.ajax.js" type="text/javascript"></script>
    <script src="path/to/mdcnette.ajax.snackbar.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(function () {
    	$.nette.init();
    });
    </script>
    <!--/ IF AJAX-->
    
    <!--IF NO AJAX-->
    <script src="path/to/mdcnette.noajax.snackbar.js" type="text/javascript"></script>
    <!--/IF NO AJAX-->

</body>
</html>
```
*After this you can start using material snackbar!*

## Usage
```php
$this->snackbar(
    "<message>",                //Message
    ["<template>"],             //Template -> viz template
    [array(<options>)]          //Options -> viz options table
    );
```

## Options

For better understanding, look for official demo [HERE](https://material-components-web.appspot.com/snackbar.html)

| Name              | Description                                           | Default   | type      | Good to know                      |
| -------------     |:-----------------------------------------------------:|:---------:|:---------:|----------------------------------:|
| `wait`            | Time for snackbar dismiss                             | 2750 ms   | integer   |                                   |
| `ajax`            | if action is fired with ajax                          | false     | boolean   |                                   |
| `action`          | Url for action                                        | false     | String    | Must be set `actionText` option   |
| `actionText`      | Text for action button                                | false     | String    | Must be set `action` option       |
| `multiline`       | Snackbar is taller to for longer message              | false     | boolean   |                                   |
| `rtl`             | Right to left                                         | false     | boolean   |                                   |
| `actionDismiss`   | On click action will dismiss (only with ajax)         | true      | boolean   | OR wait for timeout               |
| `alignStart`      | Shows snackbar on the left side of page               | false     | boolean   | works with rtl                    | 

You can set up default options in config file for all snackbars.
```neon
extensions:
    mdcsnackbar: MDCNette\Snackbar\DI\SnackbarExtension
    
mdcsnackbar:
    wait: 5000 # default 4000 (must be same or higher)
    ajax: true # default false
    alignStart: true # default false
    # etc...
``` 

## Template
MDCNette Snackbar comes with possibility to add custom predefined options template.

In config you can add this template:
```neon
extensions:
    mdcsnackbar: MDCNette\Snackbar\DI\SnackbarExtension
    
mdcsnackbar:
    rtl: true # 'rtl' is now set for all templates
    error:
        wait: 7500
    left:
        alignStart: true
    right:
        alignStart: true
        rtl: true
```  
And usage may look like this:
```php
$this->snackbar('FATAL ERROR', 'error');
// You can add custom options to every template as well
$this->snackbar('VERY LOOOOOOOOOOOOOONG FATAL ERROR', 'error', ['multiline' => true]);
```

## Examples
```php
$this->snackbar('You are logged in as guest@example.com');
```
```php
$this->snackbar('User added', NULL,
    [
        'wait'         => 5000,
        'action'       => $this->link('undo!'),
        'actionButton' => 'undo',
        'alignStart    => true
    ]);
```