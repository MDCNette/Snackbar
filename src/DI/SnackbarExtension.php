<?php
/**
 * MDCNette Snackbar
 *
 * @link        https://gitlab.com/MDCNette/Snackbar
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Snackbar\DI;


use MDCNette\Snackbar\Components\Control;
use MDCNette\Snackbar\Components\IControl;
use MDCNette\Snackbar\Components\SnackbarManager;
use MDCNette\Snackbar\Components\TemplateManager;
use MDCNette\Snackbar\Events\OnResponseHandler;
use MDCNette\Snackbar\Storage\Session;
use Nette\DI\CompilerExtension;

class SnackbarExtension extends CompilerExtension {



	public function loadConfiguration() {
		$templates = $this->prepareConfig($this->config);
		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('template'))
			->setFactory(TemplateManager::class, [$templates]);

		$builder->addDefinition($this->prefix('session'))
			->setFactory(Session::class);

		$builder->addFactoryDefinition($this->prefix('control'))
			->setImplement(IControl::class)
			->getResultDefinition()
			->setFactory(Control::class);

		$builder->addDefinition($this->prefix('snackbar'))
			->setFactory(SnackbarManager::class)
			->addSetup('setDefaults', [$this->config]);

		$onResponseHandler = $this->prefix('onResponseHandler');
		$builder->addDefinition($onResponseHandler)
			->setFactory(OnResponseHandler::class);
		$application = $builder->getDefinition('application');
		$application->addSetup('$service->onResponse[] = ?', ['@' . $onResponseHandler]);
	}

	private function prepareConfig(&$config): array {
		$templates = [];
		foreach ($config as $template => $options) {
			if (\is_array($options)) {
				$templates[$template] = $options;
				unset($config->$template);
			}
		}

		return $templates;
	}
}