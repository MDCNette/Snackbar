<?php
/**
 * MDCNette Snackbar
 *
 * @link        https://gitlab.com/MDCNette/Snackbar
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Snackbar\Exceptions;


/**
 * Base class for snackbar exceptions.
 * @package MDCNette\Snackbar\Exceptions
 */
class SnackbarExceptions extends \Exception {

}

/**
 * Exception is connected to template manager.
 * @package MDCNette\Snackbar\Exceptions
 */
class TemplateExceptions extends SnackbarExceptions {

}