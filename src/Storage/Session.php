<?php
/**
 * MDCNette Snackbar
 *
 * @link        https://gitlab.com/MDCNette/Snackbar
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Snackbar\Storage;


use Nette\Http\SessionSection;
use Nette\InvalidArgumentException;

/**
 * Class Session
 * @package MDCNette\Dialog\Storage
 * @author  Tomáš Němec <nemi@skaut.cz>
 */
class Session {

	/** Session KEY for snackbars */
	const SNACKBARS = 'snackbars';

	/** @var  SessionSection */
	private $section;

	/**
	 * Storage constructor.
	 *
	 * @param \Nette\Http\Session $session
	 * @throws InvalidArgumentException
	 */
	public function __construct(\Nette\Http\Session $session) {
		$this->section = $session->getSection('mdcnette.snackbar');
	}

	/**
	 * @param string $key
	 * @param $value
	 */
	public function set(string $key, $value): void {
		$this->section->$key = $value;
	}

	/**
	 * @param string $key
	 * @param bool $default
	 * @return bool|mixed
	 */
	public function get(string $key, $default = false) {
		return isset($this->section->$key) ? $this->section->$key : $default;
	}

	/**
	 * @param string $key
	 */
	public function clear(string $key): void {
		unset($this->section->$key);
	}


	public function clearAll(): void {
		$this->section->remove();
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function &__get($name) {
		$value = $this->get($name);
		return $value;
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return void
	 */
	public function __set($name, $value) {
		$this->set($name, $value);
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public function __isset($name) {
		return isset($this->section->$name);
	}

	/**
	 * @param string $name
	 * @return void
	 */
	public function __unset($name) {
		$this->clear($name);
	}
}