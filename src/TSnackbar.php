<?php
/**
 * MDCNette Snackbar
 *
 * @link        https://gitlab.com/MDCNette/Snackbar
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Snackbar;


use MDCNette\Snackbar\Components\Control;
use MDCNette\Snackbar\Components\IControl;
use MDCNette\Snackbar\Components\SnackbarManager;

/**
 * Trait TSnackbar
 * @package MDCNette\Snackbar
 */
trait TSnackbar {

	/** @var  IControl */
	protected $SnackbarFactory;

	/** @var  SnackbarManager */
	private $snackbar;

	/**
	 * @param IControl $SnackbarFactory
	 * @param SnackbarManager $snackbar
	 */
	public function injectMdcSnackbarFactory(IControl $SnackbarFactory, SnackbarManager $snackbar) {
		$this->SnackbarFactory = $SnackbarFactory;
		$this->snackbar = $snackbar;
	}

	/**
	 * @return Control
	 */
	public function createComponentSnackbar(): Control {
		return $this->SnackbarFactory->create();
	}

	/**
	 * @param $message
	 * @param string|NULL $template
	 * @param array $options
	 * @throws Exceptions\SnackbarExceptions
	 * @throws Exceptions\TemplateExceptions
	 */
	public function snackbar($message, string $template = NULL, array $options = []) {
		$this->snackbar->setSnackbar($message, $template, $options);
		$this->redrawControl();
	}
}