<?php
/**
 * MDCNette Snackbar
 *
 * @link        https://gitlab.com/MDCNette/Snackbar
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Snackbar\Events;

use MDCNette\Snackbar\Components\Snackbar;
use MDCNette\Snackbar\Storage\Session;

class OnResponseHandler {

	/**
	 * @var Session
	 */
	private $storage;

	/**
	 * @param Session $storage
	 */
	public function __construct(Session $storage) {
		$this->storage = $storage;
	}

	/**
	 * @return void
	 */
	public function __invoke() {
		/** @var Snackbar[] $snackbars */
		$snackbars = $this->storage->get(Session::SNACKBARS, []);

		foreach ($snackbars as $key => $snackbar) {
			if ($snackbar->isDisplayed()) {
				unset($snackbars[$key]);
			}
		}
		$this->storage->set(Session::SNACKBARS, $snackbars);
	}
}
