<?php
/**
 * MDCNette Snackbar
 *
 * @link        https://gitlab.com/MDCNette/Snackbar
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Snackbar\Components;


use MDCNette\Snackbar\Storage\Session;
use Nette\Application\UI\Presenter;

/**
 * Class Control
 * @package MDCNette\Snackbar
 */
class Control extends \Nette\Application\UI\Control {

	/** @var  Session */
	private $session;

	/**
	 * Control constructor.
	 *
	 * @param Session $session
	 */
	public function __construct(Session $session) {
		$this->session = $session;
		$this->monitor(Presenter::class, function () {
			$this->redrawControl();
		});
	}

	public function render(): void {
		$this->template->setFile(__DIR__ . '/templates/snackbar.latte');
		$this->template->snackbars = $this->session->get(Session::SNACKBARS, []);
		$this->template->render();
	}

}