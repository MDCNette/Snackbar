<?php
/**
 * MDCNette Snackbar
 *
 * @link        https://gitlab.com/MDCNette/Snackbar
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types = 1);

namespace MDCNette\Snackbar\Components;


use MDCNette\Snackbar\Exceptions\TemplateExceptions;

/**
 * Class TemplateManager
 * @package MDCNette\Snackbar\Components
 */
class TemplateManager {

    /**
     * @var array
     */
    private $templates;

    /** @var  array */
    private $templateTypes;

    /**
     * TemplateManager constructor.
     *
     * @param array $templates
     */
    public function __construct(array $templates) {
        $this->templates = $templates;
        $this->setTemplateTypes($templates);
    }

    /**
     * @param string $template
     *
     * @return array
     * @throws TemplateExceptions
     */
    public function getOptions(string $template): array {
        if ($this->templateTypes && \in_array($template, $this->templateTypes, TRUE)) {
            return $this->templates[ $this->templateTypes[ array_search($template, $this->templateTypes, TRUE) ] ];
        }

        throw new TemplateExceptions('Template type \'' . $template . '\' was not found.');
    }

    /**
     * @param array $templates
     */
    public function setTemplateTypes(array $templates) {
        foreach ($templates as $type => $config) {
            $this->templateTypes[] = $type;
        }
    }
}