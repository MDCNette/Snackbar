<?php
/**
 * MDCNette Snackbar
 *
 * @link        https://gitlab.com/MDCNette/Snackbar
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Snackbar\Components;


use Nette\SmartObject;

/**
 * Class Snackbar
 * @package MDCNette\Snackbar
 * @property bool $ajax
 * @property bool $alignStart
 * @property bool $displayed
 * @property bool $rtl
 * @property bool $actionDismiss
 * @property bool $multiline
 * @property int $wait
 * @property string $actionText
 * @property string $action
 * @property string $message
 */
class Snackbar {

	use SmartObject;

	/** @var  string */
	protected $message;

	/** @var  string */
	protected $action;

	/** @var  string */
	protected $actionText;

	/** @var int */
	protected $wait;

	/** @var  bool */
	protected $actionDismiss;

	/** @var bool */
	protected $multiline;

	/** @var bool */
	protected $alignStart;

	/** @var bool */
	protected $rtl;

	/** @var  bool */
	protected $displayed = FALSE;

	/** @var bool */
	protected $ajax;

	/**
	 * @return string|null
	 */
	public function getAction(): ?string {
		return $this->action;
	}

	/**
	 * @param string|null $action
	 */
	public function setAction(?string $action) {
		$this->action = $action;
	}

	/**
	 * @return bool
	 */
	public function isMultiline(): bool {
		return $this->multiline;
	}

	/**
	 * @param bool $multiline
	 */
	public function setMultiline(bool $multiline) {
		$this->multiline = $multiline;
	}

	/**
	 * @return string|null
	 */
	public function getActionText(): ?string {
		return $this->actionText;
	}

	/**
	 * @param string|null $actionText
	 */
	public function setActionText(?string $actionText) {
		$this->actionText = $actionText;
	}

	/**
	 * @return int
	 */
	public function getWait(): int {
		return $this->wait;
	}

	/**
	 * @param int $wait
	 */
	public function setWait(int $wait) {
		$this->wait = $wait;
	}

	/**
	 * @return bool
	 */
	public function isActionDismiss(): bool {
		return $this->actionDismiss;
	}

	/**
	 * @param bool $actionDismiss
	 */
	public function setActionDismiss(bool $actionDismiss) {
		$this->actionDismiss = $actionDismiss;
	}

	/**
	 * @return string
	 */
	public function getMessage(): string {
		return $this->message;
	}

	/**
	 * @param string $message
	 */
	public function setMessage(string $message) {
		$this->message = $message;
	}

	/**
	 * @return bool
	 */
	public function isAlignStart(): bool {
		return $this->alignStart;
	}

	/**
	 * @param bool $alignStart
	 */
	public function setAlignStart(bool $alignStart) {
		$this->alignStart = $alignStart;
	}

	/**
	 * @return bool
	 */
	public function isRtl(): bool {
		return $this->rtl;
	}

	/**
	 * @param bool $rtl
	 */
	public function setRtl(bool $rtl) {
		$this->rtl = $rtl;
	}

	/**
	 * @return bool
	 */
	public function isDisplayed(): bool {
		return $this->displayed;
	}

	public function setDisplayed() {
		$this->displayed = TRUE;
	}

	/**
	 * @return bool
	 */
	public function isAjax(): bool {
		return $this->ajax;
	}

	/**
	 * @param bool $ajax
	 */
	public function setAjax(bool $ajax) {
		$this->ajax = $ajax;
	}

	/**
	 * @return array
	 */
	public function __sleep() {

		return ['message', 'rtl', 'alignStart', 'actionDismiss', 'actionText', 'multiline', 'wait', 'action', 'ajax', 'displayed'];
	}
}