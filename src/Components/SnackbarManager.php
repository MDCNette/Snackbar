<?php
/**
 * MDCNette Snackbar
 *
 * @link        https://gitlab.com/MDCNette/Snackbar
 * @copyright   Copyright 2017 Tomáš Němec
 * @License     viz license.md
 * @author      Tomáš Němec <nemi@skaut.cz>
 */

declare(strict_types=1);

namespace MDCNette\Snackbar\Components;


use MDCNette\Snackbar\Exceptions\SnackbarExceptions;
use MDCNette\Snackbar\Storage\Session;
use Nette\Schema\Helpers;
use Nette\SmartObject;
use Nette\Utils\Arrays;
use Tracy\Debugger;

/**
 * Class Snackbar
 * @package MDCNette\Snackbar
 */
class SnackbarManager {

	use SmartObject;

	/** @var  Session */
	protected $session;
	/**
	 * @var array|object
	 */
	private $defaults;
	/**
	 * @var TemplateManager
	 */
	private $templateManager;

	/**
	 * Snackbar constructor.
	 *
	 * @param Session $session
	 * @param TemplateManager $templateManager
	 */
	public function __construct(Session $session, TemplateManager $templateManager) {
		$this->session = $session;
		$this->templateManager = $templateManager;
	}

	/**
	 * @param string $message
	 * @param string|NULL $template
	 * @param array $options
	 * @throws SnackbarExceptions
	 * @throws \MDCNette\Snackbar\Exceptions\TemplateExceptions
	 */
	public function setSnackbar(string $message, string $template = NULL, array $options = []) {
		$snackbars = $this->session->get(Session::SNACKBARS, []);

		if ($template) {
			$options = $this->templateManager->getOptions($template);
			$options = Arrays::mergeTree($options,(array) $this->defaults);
		} else {
			$options = Arrays::mergeTree($options, (array) $this->defaults);
		}

		$this->checkOptions($options);
		$snackbar = new Snackbar();
		$snackbar->setMessage($message);
		$snackbar->setAction(Arrays::getRef($options, 'action'));
		$snackbar->setAjax(Arrays::get($options, 'ajax', false));
		$snackbar->setRtl(Arrays::get($options, 'rtl', false));
		$snackbar->setWait(Arrays::get($options, 'wait', 4000));
		$snackbar->setAlignStart(Arrays::get($options, 'alignStart', false));
		$snackbar->setMultiline(Arrays::get($options, 'multiline', false));
		$snackbar->setActionText(Arrays::getRef($options, 'actionText'));
		$snackbar->setActionDismiss(Arrays::get($options, 'actionDismiss', true));

		if (!$this->isUnique($snackbar, $snackbars)) {
			$snackbars[] = $snackbar;
		}

		$this->session->set(Session::SNACKBARS, $snackbars);
	}

	/**
	 * @param array $options
	 *
	 * @throws SnackbarExceptions
	 */
	private function checkOptions(array $options) {
		if (Arrays::get($options, 'action', false) xor Arrays::getRef($options, 'actionText') !== NULL) {
			throw new SnackbarExceptions('Both options \'actionText\' and \'action\' must be set.');
		}
	}

	/**
	 * @param Snackbar $snackbar
	 * @param Snackbar[] $snackbars
	 *
	 * @return bool
	 */
	private function isUnique(Snackbar $snackbar, array $snackbars): bool {
		foreach ($snackbars as $snackbarInSession) {
			if (!$snackbarInSession->isDisplayed() && $snackbarInSession->getMessage() === $snackbar->getMessage()) {
				return TRUE;
			}
		}

		return FALSE;
	}

	/**
	 * @param array|object $defaults
	 */
	public function setDefaults($defaults) {
		$this->defaults = $defaults;
	}

	private function getAttribute($array, string $index) {
		if (!isset($attribute[$index])) {
			return null;
		}
	}
}